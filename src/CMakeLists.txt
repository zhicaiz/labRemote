#
# Check for recent compiler
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.2)
    message(FATAL_ERROR "Clang version must be at least 3.2!")
  endif()
else()
  message(WARNING "You are using an unsupported compiler! Compilation has only been tested with Clang and GCC.")
endif()

if(CMAKE_BUILD_TYPE MATCHES Debug)
  add_compile_options(-g)
else()
  add_compile_options(-O2)
endif()

#
# Check dependencies

# Find libftdi1
find_package( libftdi1 )

# Find libmpsse
find_package( libmpsse )

# Find lusb-1.0
find_package( libusb-1.0 )

if ( (${LIBFTDI1_FOUND}) AND (${LIBMPSSE_FOUND}) )
  message(STATUS "Enabling FTDI and MPSSE support")
  set(ENABLE_FTDI 1)
  add_definitions(-DENABLE_FTDI)
else()
  message(STATUS "Disabling FTDI code due to missing libftdi1 or libmpsse:")
  message(STATUS "  LIBFTDI1_FOUND = ${LIBFTDI1_FOUND}")
  message(STATUS "  LIBMPSSE_FOUND = ${LIBMPSSE_FOUND}")
endif()

if (${LIBUSB_1_FOUND})
    set(ENABLE_USB 1)
    add_definitions(-DLIBUSB_ENABLED)
endif()

# Find nlohmann json or use integrated one
if (USE_EXTERNAL_JSON)
  find_package(nlohmann_json 3.2.0 REQUIRED)
  set(JSON_LIBS "nlohmann_json")
else()
  message(STATUS "Using internal JSON library")
  set(JSON_BuildTests OFF CACHE INTERNAL "")
  set(JSON_Install OFF CACHE INTERNAL "")
  add_subdirectory(exts/nlohmann_json EXCLUDE_FROM_ALL)
  set(JSON_LIBS "nlohmann_json::nlohmann_json")
endif()

# Find influxdb-cpp or use integrated one
if (NOT USE_EXTERNAL_INFLUXDBCPP)
  SET(INFLUXDBCPP_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/exts/influxdb-cpp)
endif()
find_package(influxdbcpp)

# Find libgpib
find_package ( libgpib )

if ( (${LIBGPIB_FOUND}) )
  set(ENABLE_GPIB 1)
else()
  message(STATUS "Disabling linux-GPIB code due to missing libgpib (LIBGPIB_FOUND = ${LIBGPIB_FOUND})")
endif()

#
# Add libraries
add_subdirectory(libPS)
add_subdirectory(libMeter)
add_subdirectory(libLoad)
add_subdirectory(libCom)
add_subdirectory(libDevCom)
add_subdirectory(libGalil)
add_subdirectory(libImageRec)
add_subdirectory(libZaber)
add_subdirectory(libWaferProb)
add_subdirectory(libScope)
add_subdirectory(libWaveFormGen)
add_subdirectory(libUtils)
add_subdirectory(libEquipConf)
add_subdirectory(libDataSink)
add_subdirectory(libChiller)

#
# Add binaries
add_subdirectory(examples)
add_subdirectory(tools)

if (USE_PYTHON)
  find_package(pybind11 QUIET)
  if(pybind11_FOUND)
    message(STATUS "Using external pybind11")
  else()
    message(STATUS "Using internal pybind11")
    add_subdirectory(exts/pybind11 EXCLUDE_FROM_ALL)
  endif()

  find_package(pybind11_json QUIET)
  if(pybind11_json_FOUND)
    message(STATUS "Using external pybind11_json")
  else()
    message(STATUS "Using internal pybind11_json")
    add_subdirectory(exts/pybind11_json EXCLUDE_FROM_ALL)
  endif()

  # Add python lib
  message(STATUS "Building labRemote python module")
  add_subdirectory(labRemote)

  set(PP [=[\$$PYTHONPATH]=])
  add_custom_target(do_always ALL DEPENDS labRemote
                    COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --cyan
                    "Python bindings for ${PYTHON_EXECUTABLE} were built, use: export PYTHONPATH=${PROJECT_BINARY_DIR}/lib:${PP}")
endif()
