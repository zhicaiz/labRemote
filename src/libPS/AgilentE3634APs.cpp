#include "AgilentE3634APs.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE3634APs)

AgilentE3634APs::AgilentE3634APs(const std::string& name) :
AgilentPs(name, {"E3633A", "E3634A"}, 1)
{ }
