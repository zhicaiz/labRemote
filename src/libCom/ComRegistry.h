#include "ClassRegistry.h"
#include "ICom.h"

#include <string>
#include <vector>
#include <memory>
#include <functional>

// Configuration helpers and registry for specific Com implementations
namespace EquipRegistry
{
  /** Register new Com type into registry */
  bool registerCom(const std::string& model,
		   std::function<std::shared_ptr<ICom>()> f);

  /** Get new instance of given power supply type */
  std::shared_ptr<ICom> createCom(const std::string& model);

  /** List available Com types */
  std::vector<std::string> listCom();  
}

#define REGISTER_COM(model)						\
  static bool _registered_##model =					\
    EquipRegistry::registerCom(#model,					\
			       std::function<std::shared_ptr<ICom>()>([]() { return std::make_shared<model>(); }));
