#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "Logger.h"
#include "EquipConf.h"
#include "PowerSupplyChannel.h"
#include "IDataSink.h"

//------ SETTINGS
uint32_t tsleep = 10;
std::string configFile;
std::vector<std::string> channelNames;
//---------------

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " configfile.json channelname0 [channelname1 ...]" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --time        Milliseconds between reads (default: " << tsleep << ")" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char*argv[])
{

  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  //Parse command-line
  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"time" ,    no_argument      , 0,  't' },
      {"debug",    no_argument      , 0,  'd' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "t:d", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 't':
	tsleep = std::atoi(optarg);
	break;
      case 'd':
	logIt::incrDebug();
	break;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(optind>argc-2)
    {
      logger(logERROR) << "Missing positional arguments.";
      usage(argv);
      return 1;
    }

  configFile  = argv[optind++];
  while(optind!=argc)
    channelNames.push_back(argv[optind++]);

  logger(logDEBUG) << "Settings:";
  logger(logDEBUG) << " Config file: " << configFile;
  logger(logDEBUG) << " Channels: ";
  for(const std::string& channelName : channelNames)
  {
    logger(logDEBUG) << "\t" << channelName;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  //
  // Create hardware database
  EquipConf hw;
  hw.setHardwareConfig(configFile);

  // Create a process per channel.
  std::string myChannelName;
  pid_t pid; // Used to indicate child vs parent process
  std::vector<pid_t> cpids;
  for( const std::string& channelName : channelNames )
  {
    myChannelName=channelName;
    pid=fork();
    if(pid==0)
      { // Child process, start spamming
	srand(time(NULL)); // Initilize random number with different time from other children
	break;
      }
    cpids.push_back(pid);

    // Some time for child process to print out test parameters
    // before launching the next child process
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }

  if(pid==0)
  { // Job of child process is to spam power supply until terminated

    logger(logDEBUG) << "Retrieve channel " << myChannelName;
    std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(myChannelName);
    if(PS==nullptr)
      {
	logger(logERROR) << myChannelName << ": Channel not found";
	return 1;
      }

    uint32_t myvolt=rand()%6;

    logger(logINFO) << myChannelName << ": testing voltage = " << myvolt;

    PS->setVoltageLevel(myvolt);

    while(!quit)
    {
      uint32_t nowvolt=PS->getVoltageLevel();
      if(nowvolt!=myvolt)
      { logger(logERROR) << myChannelName << ": Read out " << nowvolt; }
      PS->setVoltageLevel(myvolt);

      std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
    }
  }
  else
  { // Parent process waits for PID's
    int status;
    for(pid_t cpid : cpids)
    {
      waitpid(cpid, &status, 0);
      logger(logINFO) << "Spam process " << cpid << " completed with status code " << status;
    }
  }
  
  return 0;
}
