#include "PolySciLM.h"
#include "Logger.h"

#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <string>
#include <memory>

float target_temp=15;
std::string device="";
speed_t baud=B9600;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -p, --port ADDR     Set the address of the port connected to the chiller" << std::endl;  
  std::cerr << " -b, --baud BAUD     Set the baud rate of the chiller (default: 9600)" << std::endl;  
  std::cerr << " -t, --temperature T Set the target temperature of the chiller (default: " << target_temp << ")" << std::endl;  
  std::cerr << " -d, --debug         Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help          List the commands and options available"  << std::endl;
}

int main(int argc, char* argv[]) 
{ 
  // Parse command-line
    if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"port",  required_argument, 0,  'p' },
      {"baud",  required_argument, 0,  'b' },
      {"temperature",  required_argument, 0,  't' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "p:b:t:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'p':
	device = optarg;
	break;
      case 'b': {
        std::string baud_str=optarg;
        if(baud_str=="2400") baud=B2400;
        else if(baud_str=="4800") baud=B4800;
        else if(baud_str=="9600") baud=B9600;
        else if(baud_str=="19200") baud=B19200;
        else {
          std::cerr << "Invalid baud rate '" << optarg << "' supplied. Use either 2400, 4800, 9600, or 19200. Aborting." << std::endl;
          return 1;
        }
        break;
      }
      case 't':
        try
        {
	    target_temp = std::stof(optarg);
        }
        catch(const std::invalid_argument& e)
        {
	    std::cerr << "Temperature must be a number. "<< optarg << "supplied. Aborting." << std::endl;
	    return 1;
        }
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(device=="") {
      std::cerr << "No device port specified for the Arduino. Aborting." <<std::endl;
      usage(argv);
      return 1;
  }

  logger(logDEBUG) << "Device port: " << device;
  
  std::shared_ptr<TextSerialCom> com = 
    std::make_shared<TextSerialCom>(device,baud);
  com->setTermination("\r");

  PolySciLM chiller;
  chiller.setCom(com);
  chiller.init();
  chiller.turnOn();
  chiller.setTargetTemperature(target_temp);

  while(true) {
    float temp=chiller.measureTemperature();
    logger(logINFO) << "Temperature: " << temp;
    float stemp=chiller.getTargetTemperature();
    logger(logINFO) << "Set Temperature: " << stemp;
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}
